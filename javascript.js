'use strict';
document.getElementById("SendButton");
function loadPage() {
    console.log("Page loaded!");
    let button =  document.getElementById("SendButton");
    button.addEventListener('click', SendPost)
};
function SendPost() {
    console.log("Post sent!");
    let thread = document.getElementById("Thread");
    let NewPost = document.createElement("dt");
    let author = document.getElementById("Author");
    let content = document.getElementById("Content");
    NewPost.innerText = `${author.value} : ${content.value}`;
    thread.appendChild(NewPost)
}
window.addEventListener('load', loadPage);
setInterval(SendPost, 30);